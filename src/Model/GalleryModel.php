<?php
namespace Bistriteanul\Api\Model;

use Silex\Application;

class GalleryModel extends AbstractModel
{
    public function __construct(Application $api)
    {
        parent::__construct($api);
    }

    public function findAllByArticleId($articleId)
    {
        $stmt = $this->db->executeQuery("SELECT * FROM `article_gallery` WHERE `article_id` = ?", [
            $articleId
        ]);

        return $stmt->fetch();
    }

    public function findOneByArticleId($articleId)
    {
        $stmt = $this->db->executeQuery("SELECT * FROM `article_gallery` WHERE `article_id` = ? ORDER BY `added_at` ASC LIMIT 1", [
            $articleId
        ]);

        return $stmt->fetch();
    }

    public function insertArticleGallery(array $data)
    {
        return $this->db->insert('article_gallery', $data);
    }
}