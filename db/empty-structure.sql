DROP TABLE IF EXISTS `article_gallery`;
CREATE TABLE `article_gallery` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `article_id` VARCHAR(65) NOT NULL,
  `media_type` VARCHAR(45) NOT NULL,
  `media_size` INT UNSIGNED NOT NULL,
  `media_path` TEXT NOT NULL,
  `media_link` TEXT NOT NULL,
  `added_at`  DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `index2` USING BTREE (`article_id` ASC))
ENGINE = InnoDB